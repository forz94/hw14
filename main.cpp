#include <iostream>
#include <string>

int main()
{
	std::string Text("Hello Skillbox");
	std::cout << Text << "\n";
	std::cout << Text.length() << "\n";
	std::cout << Text[0] << "\n";
	std::cout << Text[Text.length() - 1] << "\n";
}